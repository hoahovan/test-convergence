version: '3'

services:

  ##
  ## OrientDB is the main database Convergence uses to store data.
  ##
  orientdb:
    image: orientdb:3.0.37
    # restart: always
    environment:
      ORIENTDB_ROOT_PASSWORD: "password"
      ORIENTDB_OPTS_MEMORY: "-Xms1g -Xmx4g"
      JAVA_OPTS: "-Dstorage.wal.allowDirectIO=false"
    volumes:
      - ./data/orientdb/databases:/orientdb/databases
      - ./data/orientdb/backup:/orientdb/backup
    ports:
      - "2424:2424"
      - "2480:2480"

  ##
  ## A bootstrapping container that serves as the seed node for the distributed
  ## Convergence cluster.
  ##
  cluster-seed:
    image: ${CONVERGENCE_DOCKER_REPO}/convergence-cluster-seed:${CONVERGENCE_DOCKER_TAG}
    command: "-Dlog4j.configurationFile=/etc/convergence/log4j2.xml"
    # restart: always
    volumes:
       - ./config/convergence/log4j2.xml:/etc/convergence/log4j2.xml
    environment:
      JAVA_OPTS: "-Xmx64m -Xss512k -XX:CICompilerCount=1 -XX:-TieredCompilation"
      REMOTING_EXTERNAL_HOSTNAME: cluster-seed
      CLUSTER_SEED_NODES: cluster-seed

  autoheal:
    restart: always
    image: willfarrell/autoheal
    environment:
      - AUTOHEAL_CONTAINER_LABEL=all
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

  ##
  ## The main Convergence Server. In this compose file this single node boots all
  ## server roles.
  ##
  server:
    image: ${CONVERGENCE_DOCKER_REPO}/convergence-server:${CONVERGENCE_DOCKER_TAG}
    # restart: always
    volumes:
       - ./config/convergence/log4j2.xml:/opt/convergence-server/conf/log4j2.xml
       - ./config/convergence/convergence-server-staging.conf:/opt/convergence-server/conf/convergence-server.conf
    links:
     - cluster-seed
     - orientdb
    logging:
      driver: "json-file"
      options:
        max-size: "10m"
        max-file: "3"
    healthcheck:
      test: ["CMD", "curl", "-f", "-X", "POST", "http://localhost:8081/auth/login", "-H", "content-type: application/json", "-d", '{"username": "healthcheck", "password": "M8:8`8~`RR9c.v6="}']
      interval: 5m
      timeout: 10s
      retries: 3

  ##
  ## Serves the Convergence Admin Console
  ##
  admin-console:
    image: ${CONVERGENCE_DOCKER_REPO}/convergence-admin-console:${CONVERGENCE_DOCKER_TAG}
    # restart: always
    environment:
      CONVERGENCE_SERVER_REALTIME_API: "http://${DOCKER_HOSTNAME_STAGING}/realtime/"
      CONVERGENCE_SERVER_REST_API: "http://${DOCKER_HOSTNAME_STAGING}/rest/"
      CONVERGENCE_CONSOLE_BASE_URL: "/console/"


  ##
  ## The proxy simply creates a single HTTP/HTTP Ingress and forwards traffic
  ## to the appropriate container.
  ##
  proxy:
    image: nginx:1.21.1-alpine
    # restart: always
    volumes:
      - ./config/nginx/default_staging.conf:/etc/nginx/conf.d/default.conf
      - ./config/nginx/html:/usr/share/nginx/html
    depends_on:
      - orientdb
      - server
      - admin-console
    ports:
      - "443:443"
      - "80:80"
    command: "/bin/sh -c 'while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g \"daemon off;\"'"
